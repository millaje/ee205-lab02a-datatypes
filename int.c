///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.c
/// @version 1.0
///
/// Print the characteristics of the "int", and "unsigned int" datatypes.
///
/// @author Jeraldine Milla <millaje@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   19 Jan 2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <limits.h>

#include "datatypes.h"
#include "int.h"


///////////////////////////////////////////////////////////////////////////////
/// int

/// Print the characteristics of the "int" datatype
void doInt() {
   printf(TABLE_FORMAT_INT, "int", sizeof(int)*8, sizeof(int), INT_MIN, INT_MAX);
}


/// Print the overflow/underflow characteristics of the "int" datatype
void flowInt() {
   int overflow = INT_MAX;
   printf("int overflow:  %d + 1 ", overflow++);
   printf("becomes %d\n", overflow);

   int underflow = INT_MIN;
   printf("int underflow:  %d - 1 ", underflow--);
   printf("becomes %d\n", underflow);
}




///////////////////////////////////////////////////////////////////////////////
/// unsigned int

/// Print the characteristics of the "unsigned int" datatype
void doUnsignedInt() {
   printf(TABLE_FORMAT_UINT, "unsigned int", sizeof(int)*8, sizeof(int), 0, UINT_MAX);  // ???See the comment next to UCHAR_MAX in limits.h
}



/// Print the overflow/underflow characteristics of the "unsigned Int" datatype
void flowUnsignedInt() {
   unsigned int overflow = UINT_MAX;
   printf("unsigned int overflow:  %u + 1 ", overflow++);
   printf("becomes %u\n", overflow);

   unsigned int underflow = 0;  // ???Note:  See limits.h UCHAR_MAX... there's no UCHAR_MIN, it's 0.
   printf("unsigned int underflow:  %u - 1 ", underflow--);
   printf("becomes %u\n", underflow);
}
